import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Класс нахождения идентификатора в коде
 *
 * @author Sumka Andrey 18it18
 */

public class Exam {
    public static void main(String[] args) {
        System.out.println("Ввыводим текст кода в консоль: ");
        String result = " ";
        try {
            System.out.println(reader());
            result = reader();
        } catch (IOException wrong) {
            System.out.println("Файл не найден!");
        }

        System.out.println("\n" + "Выводим все идентификаторы: ");
        System.out.println(reg1(result));

        System.out.println("\n" + "Выводим уникальные идентификаторы: ");
        System.out.println(reg2( result));
    }

    /**
     * Метод нахождения и чтения файла
     *
     * @return вывод программы
     * @throws IOException
     */
    static String reader() throws IOException {
        Path path = Paths.get("C:\\Users\\sumka\\IdeaProjects\\Exams\\src\\Test.java");
        List<String> list = Files.readAllLines(path, UTF_8);

        return String.valueOf(list);
    }

    /**
     * Метод нахождения всех идентификаторов
     *
     * @param result
     * @return выводим идентификаторы
     */
    static String reg1(String result) {
        Pattern pattern = Pattern.compile("([A-Za-z$_](\\w*))");
        Matcher matcher = pattern.matcher(result);
        String answer = "";
        while (matcher.find()) {
            answer += matcher.group();
        }
        return answer;
    }

    /**
     * Метод нахождения уникальных идентификаторов
     *
     * @param result
     * @return выводим найденные идентификаторы в строку
     */
    private static String reg2(String result) {
        Pattern pattern = Pattern.compile("([A-Za-z$_](\\w*))");
        Matcher matcher = pattern.matcher(result);
        HashSet<String> answer = new HashSet<>();
        while (matcher.find()) {
            answer.add(matcher.group());
        }
        return answer.toString();
    }
}
